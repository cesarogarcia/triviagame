

/******************************MOSTRAR----PANTALLA---OCULTAR********************************* */
let section_inicio = document.getElementById("inicio")
let section_preguntas = document.getElementById("preguntas")
let section_score = document.getElementById("score")
let butonFutbal = document.getElementById("botones_temas_futbol")
let butonBk = document.getElementById("botones_temas_basket")
/****************************************POP-UP***************************************************/
let section_pop_ok = document.getElementById("pop-up-ok")
let overlay = document.getElementById('overlay')
let section_pop_basura = document.getElementById("pop-up-cagada")
/*********************************VARIABLES ARRAY PREGUNTAS Y RESPYESESTAS************************* */

let preguntasfb = [
  {
    pregunta: "<h2>¿sabes cómo se llama popularmente al torneo español de la Copa del Rey?</h2>",
    respuesta: "Torneo del OK",
    opciones: ["Torneo  Español", "La corona.", "Torneo del Caos", "Torneo del OK"]

  },

  {
    pregunta: "<h2>¿Cuántas copas de la Liga tiene el Sevilla FC?</h2>",
    respuesta: "Una",
    opciones: ["Cero", "Una", "Dos", "Tres"]

  },

  {
    pregunta: "<h2>¿Cuál es el futbolista que más goles ha marcado en la historia de la Liga desde 1928?</h2>",
    respuesta: "Messi",
    opciones: ["Raúl", "Zarra", "Messi", "Cristiano Ronaldo"],

  },

  {
    pregunta: "<h2>La temporada más goleadora de la historia de la Liga fue la de 1996-97. ¿Cuántos goles se marcaron?</h2>",
    respuesta: "1271",
    opciones: ["756", "1271", "1045", "934"],

  },

  {
    pregunta: "<h2>El primer Clásico de la historia.</h2>",
    respuesta: "Fue ganado por el Barcelona 3-1",
    opciones: ["Fue ganado por el Barcelona 3-1", "Fue ganado por el Madrid 4-2", "Fue ganado por el Madrid 1-0", "Empataron"],

  }
]

/*********************************mostrar pantallas**************************/
section_preguntas.style.display = "none";
section_score.style.display = "none";


function mostrarPantallaFutbal() {
  section_inicio.style.display = "none";
  section_preguntas.style.display = "block";
  section_score.style.display = "none";
  mostrarpreguntasfb()
  mostrarrespuestasfb()
}

butonFutbal.addEventListener("click", mostrarPantallaFutbal)



/*********************************FUNCIONES mostrar preguntas y respuestas FB**************************/
let currentcuestionfb = 0

function mostrarpreguntasfb() {

  let preguntafb = document.getElementById("cuestion");
  preguntafb.innerHTML = preguntasfb[currentcuestionfb].pregunta;
  // currentcuestionfb++
}

function mostrarrespuestasfb() {

  let opcionesfb = document.getElementById("contenedorespuestas");
  opcionesfb.innerHTML = ""

  for (let i = 0; i < preguntasfb[currentcuestionfb].opciones.length; i++) {
    let botonradios = document.createElement('input')
    botonradios.setAttribute('type', 'radio')
    botonradios.setAttribute('name', 'respuestaRadio')
    botonradios.className = "opcionesbotonradios"
    botonradios.setAttribute('value', preguntasfb[currentcuestionfb].opciones[i])
    let textoradios = document.createElement('span')
    textoradios.innerHTML = preguntasfb[currentcuestionfb].opciones[i]

    opcionesfb.append(botonradios)
    opcionesfb.append(textoradios)
  }
}

/*********************************Boton de validar**************************/
let puntuacion = 0
let botonvalidar = document.getElementById("boton_preguntas_validar");


function haPulsadoOpc() {
  let opcionesradionull = document.querySelector("input[name='respuestaRadio']:checked");

  if (opcionesradionull === null) {
    return false
  } else {
    return true
  }
}

function tomarElcheck() {
  let elegido = document.querySelector("input[name='respuestaRadio']:checked").value;
  return elegido
}


botonvalidar.addEventListener("click", validarElcheck);

function validarElcheck() {
  let chequekok = preguntasfb[currentcuestionfb].respuesta
  let opcionMarcada = haPulsadoOpc()

  if (opcionMarcada === true) {
   
    let opcionUsuario = tomarElcheck()

    if (chequekok === opcionUsuario) {
      puntuacion++
      mostrarPop_up_bueno()
    } else {
      mostrarPop_up_malo()
    }

    pasarPregunta()

  }
  else {
    alert("elige una opcion")
  }
}

function pasarPregunta() {
  currentcuestionfb++
  mostrarpreguntasfb()
  mostrarrespuestasfb()
  section_preguntas.style.display = "block";

  if (currentcuestionfb === 4) {
    section_preguntas.style.display = "none";

  }

  if (currentcuestionfb === 4) {
    section_score.style.display = "block";

  }



}





/*********************************FUNCIONES mostrar preguntas y respuestas BK**************************/

let currentcuestionbk = 0
function mostrarpreguntasbk() {

  let preguntabk = document.getElementById("cuestion");
  preguntabk.innerHTML = preguntasbk[currentcuestionbk].pregunta;
  //currentcuestionbk++
}

/**********************Boton Exit*****************/

document.getElementById("boton_salir")
  .addEventListener("click", function (Event) {
    section_inicio.style.display = "block";
    section_preguntas.style.display = "none";
    section_score.style.display = "block";

  });

document.getElementById("boton_preguntas_validar")
  .addEventListener("click", function (Event) {

  });


document.getElementById("boton_salir2")
  .addEventListener("click", function (Event) {
    section_inicio.style.display = "block";
    section_preguntas.style.display = "none";
    section_score.style.display = "none";

  });
/****************************************************FUNCION MOSTRAR POP-UP_malo********************************/
document.getElementById("skip")
.addEventListener("click" , function (event){
  section_pop_ok.style.display = "none";
  section_preguntas.style.display = "block";

});
document.getElementById("skip2")
.addEventListener("click" , function (event){
  section_pop_basura.style.display = "none";
  section_preguntas.style.display = "block";

});
/************************************************FUNCION_LLAMAR_BOTON_SPIP***********************************/

function mostrarPop_up_bueno() {
  // section_inicio.style.display = "none";
  section_preguntas.style.display = "none";
  // section_score.style.display = "none";
  // section_score.style.display = "none";
  section_pop_ok.style.display = "block";
  // section_pop_basura.style.display = "none"

}
function ocultar_pop_up() {
  section_preguntas.style.display = "block";
  section_pop_ok.style.display = "none";


}


function mostrarPop_up_malo() {
  section_preguntas.style.display = "none";
  section_pop_basura.style.display = "block";
}
debugger
function ocultar_pop_up_malo() {

  section_preguntas.style.display = "block";
  section_pop_basura.style.display = "none";
}















/**********************pantallas_bx_bk*****************/

/*
function mostrarPantallaBk() {
  section_inicio.style.display = "none";
  section_preguntas.style.display = "block";
  section_score.style.display = "none";
  mostrarpreguntasbk()
}

butonBk.addEventListener("click", mostrarPantallaBk)

document.getElementById("botones_temas_boxeo").addEventListener("click", function (event) {
  section_inicio.style.display = "none";
  section_preguntas.style.display = "block";
  section_score.style.display = "none";

});
*/

/**********************Hell*****************/
/*function ponerInputs(preguntasfb) {
  document.getElementsByClassName("respuesta-texto")[0].innerHTML =
    preguntasfb[0].opciones[0];
    document.getElementsByClassName("respuesta-texto")[1].innerHTML =
    preguntasfb[0].opciones[1];
    document.getElementsByClassName("respuesta-texto")[2].innerHTML =
    preguntasfb[0].opciones[2];
    document.getElementsByClassName("respuesta-texto")[3].innerHTML =
    preguntasfb[0].opciones[3];
}*/

/**************************ARRAY*********************************************************/
/*
let preguntasbx = [
  {
    pregunta: "<h2>En 1936, en la Arena Nacional ocurrió el primer caso de un boxeador mexicano muerto en el ring; se trató de:“Joe” Conde</h2>",
    respuesta: "José “Joe” Becerra",
    opciones: ["Joe Conde", " Francisco “Paco” Sotelo", "Rudy Coronado", "Julio Cesar Chavez"]
  },

  {
    pregunta: "Primer boxeador mexicano que cobró un millón de dólares por una pelea",
    respuesta: "José Pipino Cuevas",
    opciones: ["José Pipino Cuevas ,  Jorge “El Maromero” Páez , Salvador Sánchez , Jorge Maravilla Martinez"]
  },

  {
    pregunta: "Barrio de La Habana, famoso en los años 40 por la gran cantidad de boxeadores que producía",
    respuesta: "José Pipino Cuevas",
    opciones: ["San Antonio", "San Nicolás", "Negro", "Estefan Kramer"]
  },

  {
    pregunta: "Fue candidato a diputado por el PST en 1982 y también actor de cine y de teatro",
    respuesta: "Rubén “Púas” Olivares",
    opciones: ["Rubén “Púas” Olivares", "Raúl “Ratón” Macías", "Luis “Kid Azteca” Villanueva", "Juan Magan"]
  },

  {
    pregunta: "Cantina donde se reúnen semanalmente numerosos ex boxeadores para recordar viejas glorias",
    respuesta: "Rubén “Púas” Olivares",
    opciones: ["La Guadalupana", "Moctezuma", "La hija de los apaches"]

  }

]
/**************************BASKET_ARRAY*********************************************************/
/*
let preguntasbk = [

  {
    pregunta: "<h2>¿Cómo le fue a España en su primer Eurobasket (1935)?</h2>",
    respuesta: "plata",
    opciones: ["plata", "oro", "bronce", "quinta"]
  },

  {
    pregunta: "¿Cuál ha sido el peor resultado de nuestra selección en este torneo?",
    respuesta: "Decimoquinto puesto turquia 1959",
    opciones: ["Decimoquinto puesto turquia 1959ta", "decimo tercero yugoslavia 1961", "undecimo URSS 1965", "finlandia 1967"]
  },

  {
    pregunta: "¿3,¿En qué Eurobasket se ganó por primera vez a la Unión Soviética para acabar logrando una plata?",
    respuesta: "Barcelona 1973",
    opciones: ["Barcelona 1973", "Alemania 1971", "Francia 1983", "Yugoslavia1975"]
  },

  {
    pregunta: "4,¿Cuál de estos seleccionadores españoles tiene el récord de participaciones en el torneo con 13??",
    respuesta: "Antonio Diaz Miguel",
    opciones: ["Antonio Diaz Miguel", "Lolo Sainz", "Sergio Scariolo", "mariano manent"]
  },

  {
    pregunta: "¿Quién anotó la canasta decisiva contra la URSS para asegurar la plata de 1983",
    respuesta: "Epi",
    opciones: ["Epi", "Fernando Martín", "Andrés Jiménez", "Corbalán"]
  }
]

*/